<?php

namespace wms\packages\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use wms\packages\models\Module;

/**
 * Search represents the model behind the search form about `wms\packages\models\Module`.
 */
class Filter extends Module
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'module_status_id', 'module_created_at', 'module_updated_at'], 'integer'],
            [['module_name', 'module_package', 'module_settings'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * 
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Module::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'module_id' => $this->module_id,
            'module_status_id' => $this->module_status_id,
            'module_created_at' => $this->module_created_at,
            'module_updated_at' => $this->module_updated_at,
        ]);

        $query->andFilterWhere(['like', 'module_name', $this->module_name])
            ->andFilterWhere(['like', 'module_package', $this->module_package])
            ->andFilterWhere(['like', 'module_settings', $this->module_settings]);

        return $dataProvider;
    }
}
