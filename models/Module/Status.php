<?php

namespace wms\packages\models\Module;

use Yii;
use wms\packages\Module;
use wms\packages\models\Module as ModuleModel;

/**
 * This is the model class for table "{{%module_statuses}}".
 *
 * @property integer $module_status_id
 * @property string $module_status_name
 * @property string $module_status_title
 *
 * @property \wms\packages\models\Module[] $modules
 */
class Status extends \yii\db\ActiveRecord
{
    const LOADED = 'loaded';
    const INSTALLED = 'installed';
    const REMOVED = 'removed';
    const FAILED = 'failed';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%module_statuses}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_status_name', 'module_status_title'], 'required'],
            [['module_status_name', 'module_status_title'], 'string', 'max' => 255],
            [['module_status_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_status_id' => Module::t('common', 'Module Status ID'),
            'module_status_name' => Module::t('common', 'Module Status Name'),
            'module_status_title' => Module::t('common', 'Module Status Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasMany(ModuleModel::class, ['module_status_id' => 'module_status_id']);
    }
}