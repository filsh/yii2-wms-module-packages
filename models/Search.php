<?php

namespace wms\packages\models;

use Yii;
use yii\base\Model;
use wms\packages\components\ProviderInterface;

class Search extends Model
{
    public $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'required'],
            [['query'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * 
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $providerParams = [];
        
        if ($this->load($params) && $this->validate()) {
            $providerParams = [
                'searchPhrase' => $this->query
            ];
        }
        return Yii::$container->get(ProviderInterface::class, [], $providerParams);
    }
}
