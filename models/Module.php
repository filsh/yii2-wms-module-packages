<?php

namespace wms\packages\models;

use Yii;
use wms\packages\Module as ModuleModule;
use wms\packages\models\Module\Status;

/**
 * This is the model class for table "{{%modules}}".
 *
 * @property integer $module_id
 * @property integer $module_status_id
 * @property string $module_name
 * @property string $module_package
 * @property string $module_settings
 * @property integer $module_created_at
 * @property integer $module_updated_at
 *
 * @property Module\Status $moduleStatus
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return ModuleModule::tablePrefix('{{%modules}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_status_id', 'module_name', 'module_package', 'module_settings', 'module_created_at', 'module_updated_at'], 'required'],
            [['module_status_id', 'module_created_at', 'module_updated_at'], 'integer'],
            [['module_settings'], 'string'],
            [['module_name', 'module_package'], 'string', 'max' => 255],
            [['module_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::class, 'targetAttribute' => ['module_status_id' => 'module_status_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_id' => ModuleModule::t('common', 'Module ID'),
            'module_status_id' => ModuleModule::t('common', 'Module Status ID'),
            'module_name' => ModuleModule::t('common', 'Module Name'),
            'module_package' => ModuleModule::t('common', 'Module Package'),
            'module_settings' => ModuleModule::t('common', 'Module Settings'),
            'module_created_at' => ModuleModule::t('common', 'Module Created At'),
            'module_updated_at' => ModuleModule::t('common', 'Module Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::class, ['module_status_id' => 'module_status_id']);
    }
}