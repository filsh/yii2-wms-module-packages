<?php

namespace wms\packages\components;

interface FinderInterface
{
    public function findByPackageName($packageName);
    
    public function findAllBySearchPhrase($searchPhrase);
}