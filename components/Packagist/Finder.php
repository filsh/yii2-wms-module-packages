<?php

namespace wms\packages\components\Packagist;

use Packagist\Api\Client;
use wms\packages\components\FinderInterface;

class Finder implements FinderInterface
{
    protected $client;
    
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    
    /**
     * @param string $packageName
     * @return \Packagist\Api\Result\Package
     */
    public function findByPackageName($packageName)
    {
        return $this->client->get($packageName);
    }

    /**
     * @param string $searchPhrase
     * @return \Packagist\Api\Result\Result[]
     */
    public function findAllBySearchPhrase($searchPhrase)
    {
        return $this->client->search($searchPhrase);
    }
}