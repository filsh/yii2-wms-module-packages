<?php

namespace wms\packages\components\Packagist;

use Packagist\Api\Result\Result;
use wms\packages\components\FinderInterface;

class Provider extends \yii\data\ArrayDataProvider
{
    public $searchPhrase;

    protected $finder;

    public function __construct(FinderInterface $finder, $config = array())
    {
        $this->finder = $finder;
        parent::__construct($config);
    }

    protected function prepareModels()
    {
        if(empty($this->allModels) && $this->searchPhrase !== null) {
            $packages = $this->finder->findAllBySearchPhrase($this->searchPhrase);
            $this->allModels = array_map([$this, 'toArray'], $packages);
        }
        return parent::prepareModels();
    }
    
    protected function toArray(Result $package = null)
    {
        if($package === null) {
            return null;
        }
        
        return [
            'id' => $package->getName(),
            'package' => $package->getName(),
            'description' => $package->getDescription(),
            'downloads' => $package->getDownloads(),
            'favers' => $package->getFavers()
        ];
    }
}