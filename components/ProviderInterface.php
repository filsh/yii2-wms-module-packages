<?php

namespace wms\packages\components;

use yii\data\DataProviderInterface;

interface ProviderInterface extends DataProviderInterface
{
}