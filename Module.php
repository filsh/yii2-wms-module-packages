<?php

namespace wms\packages;

use Yii;
use yii\web\GroupUrlRule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use wms\packages\components\ProviderInterface;
use wms\packages\components\FinderInterface;
use wms\packages\components\Packagist\Provider as PackagistProvider;
use wms\packages\components\Packagist\Finder as PackagistFinder;

class Module extends \wms\base\Module implements BootstrapInterface
{
    const VERSION = '1.0.0-dev';
    
    /**
     * @var string The prefix for user module URL.
     *
     * @See [[GroupUrlRule::prefix]]
     */
    public $urlPrefix = 'admin';
    
    /**
     * @var array The rules to be used in URL management.
     */
    public $urlRules = [
        'packages'                 => 'default/index',
        'packages/<action:(view|search|filter)>' => 'default/<action>',
    ];
    
    public function init()
    {
        parent::init();
        
        if (!isset(Yii::$app->i18n->translations['wms/packages*'])) {
            Yii::$app->i18n->translations['wms/packages*'] = [
                'class'    => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }

    public function bootstrap($app)
    {
        Yii::$container->set(ProviderInterface::class, ['class' => PackagistProvider::class]);
        Yii::$container->set(FinderInterface::class, ['class' => PackagistFinder::class]);
        
        if($app instanceof \yii\web\Application) {
            Yii::$app->urlManager->addRules([new GroupUrlRule([
                'prefix' => $this->urlPrefix,
                'rules'  => $this->urlRules,
                'routePrefix' => $this->id
            ])], false);
        }
    }
}