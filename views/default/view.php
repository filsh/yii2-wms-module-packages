<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $package Packagist\Api\Result\Package */

$this->title = $package->getName();
$this->params['breadcrumbs'][] = ['label' => Module::t('common', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-view">
    <section class="panel panel-default">
        <div class="panel-body">
            <!-- title row -->
            <div class="page-header row">
                <div class="col-xs-8">
                    <h2 class="h4"><?= $package->getDescription() ?></h2>
                </div>
                <div class="col-xs-4">
                    <p class="text-muted pull-right"><small><small><small><?= \Yii::$app->formatter->asDatetime($package->getTime()) ?></small></small></small></p>
                </div>
                <!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?= \yii\bootstrap\Button::widget([
                        'label' => '<i class="fa fa-download"></i> ' . Module::t('common', 'Install'),
                        'encodeLabel' => false,
                        'options' => [
                            'class' => 'btn btn-success pull-right'
                        ]
                    ]); ?>
                </div>
            </div>
            <!-- versions row -->
            <div class="package row">
                <div class="col-xs-8">
                    <p class="lead"><?= Module::t('common', 'Versions') ?>:</p>
                    <?php
                        $items = [];
                        $active = true;
                        /* @var $version Packagist\Api\Result\Package\Version */
                        foreach($package->getVersions() as $version) {
                            $items[] = [
                                'label' => $version->getVersion(),
                                'content' => $this->render('_version', ['version' => $version]),
                                'active' => $active
                            ];
                            
                            if($active === true) {
                                $active = false;
                            }
                        }
                    ?>
                    <div class="items">
                        <?= \kartik\tabs\TabsX::widget([
                            'items' => $items,
                            'options' => ['class' => 'nav nav-pills'],
                            'encodeLabels' => false
                        ]) ?>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead"><?= Module::t('common', 'Details') ?>:</p>
                    <?= DetailView::widget([
                        'options' => [
                            'class' => 'table'
                        ],
                        'model' => [
                            'type' => $package->getType(),
                            'time' => $package->getTime(),
                            'repository' => $package->getRepository(),
                            'installs' => $package->getDownloads()->getTotal(),
                            'stars' => $package->getFavers()
                        ],
                        'attributes' => [
                            'repository:url:' . Module::t('common', 'Repository Url'),
                            'time:datetime:' . Module::t('common', 'Last Updated'),
                            [
                                'attribute' => 'type',
                                'label' => Module::t('common', 'Type'),
                                'value' => call_user_func(function($value) {
                                    return Html::a($value, 'https://packagist.org/search?type=' . $value);
                                }, $package->getType()),
                                'format' => 'html'
                            ],
                            'installs:integer:' . Module::t('common', 'Installs'),
                            'stars:integer:' . Module::t('common', 'Stars'),
                        ]
                    ]) ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
    <!-- /.content -->
</div>