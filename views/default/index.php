<?php

use yii\helpers\Html;
use yii\grid\GridView;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $searchModel wms\packages\models\Search */
/* @var $filterModel wms\packages\models\Filter */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('common', 'Modules & Extensions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-index">
    <div class="box">
        <div class="box-header">
            <div class="row no-margin">
                <div class="col-xs-8">
                    <div class="box-tools">
                        <?php /* \yii\bootstrap\ButtonGroup::widget([
                            'buttons' => [
                                \yii\bootstrap\Button::widget([
                                    'label' => Module::t('common', 'Installed'),
                                    'options' => [
                                        'class' => 'btn btn-primary'
                                    ]
                                ]),
                                \yii\bootstrap\Button::widget([
                                    'label' => Module::t('common', 'Downloaded'),
                                    'options' => [
                                        'class' => 'btn btn-default'
                                    ]
                                ]),
                            ]
                        ]);*/ ?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <?= $this->render('_search', [
                        'model' => $searchModel
                    ]); ?>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
            'options' => [
                'class' => 'box-body table-responsive no-padding'
            ],
            'tableOptions' => [
                'class' => 'table table-hover'
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'module_id',
                'module_status_id',
                'module_name',
                'module_package',
                'module_settings:ntext',
                // 'module_created_at',
                // 'module_updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <!-- /.box -->
</div>
