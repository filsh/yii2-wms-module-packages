<?php

use yii\helpers\Html;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $model wms\packages\models\Module */

$this->title = Module::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Module',
]) . ' ' . $model->module_id;
$this->params['breadcrumbs'][] = ['label' => Module::t('common', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->module_id, 'url' => ['view', 'id' => $model->module_id]];
$this->params['breadcrumbs'][] = Module::t('common', 'Update');
?>
<div class="module-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
