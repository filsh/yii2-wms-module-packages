<?php

use yii\helpers\Html;
use yii\helpers\Url;
use \yii\web\View;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $model wms\packages\models\Search */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(); ?>

<div class="form-group has-feedback right-feedback">
    <?= $form->field($model, 'query', [
        'template' => '{input}',
        'options' => [
            'class' => 'input-group',
        ],
    ])->widget(Select2::class, [
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'placeholder' => Module::t('common', 'Search modules...'),
            'onChange' => new JsExpression('(function(input) { window.location.href = "' . Url::toRoute(['default/view', 'package' => '{{package}}']) . '".replace(encodeURI("{{package}}"), $(input).val()); })(this)'),
        ],
        'pluginOptions' => [
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => Url::toRoute(['default/search']),
                'dataType' => 'json',
                'delay' => 250,
                'data' => new JsExpression('function(params) { return {\'' . Html::getInputName($model, 'query') . '\': params.term}; }'),
                'cache' => true
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(repo) { return wms.formatters.select2package.call(this, repo);}'),
            'templateSelection' => new JsExpression('function (repo) { return repo.package; }'),
        ],
    ]) ?>
    <span class="fa fa-search form-control-feedback text-muted" aria-hidden="true"></span>
</div>
<?php /*\yii\bootstrap\Button::widget([
    'label' => '<i class="fa fa-search"></i> ' . Module::t('common', 'Advanced search'),
    'encodeLabel' => false,
    'options' => [
        'class' => 'btn btn-success pull-right'
    ]
]);*/ ?>

<?php ActiveForm::end(); ?>
