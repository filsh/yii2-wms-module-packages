<?php

use yii\helpers\Html;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $model wms\packages\models\Module */

$this->title = Module::t('common', 'Create Module');
$this->params['breadcrumbs'][] = ['label' => Module::t('common', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
