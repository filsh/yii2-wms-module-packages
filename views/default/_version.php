<?php

use yii\helpers\Html;
use wms\packages\Module;

$fnUlItems = function($data, $title) {
    $items = [];
    if(is_array($data)) {
        foreach($data as $name => $value) {
            $items[] = Html::a($name, 'https://packagist.org/packages/' . $name) . ' : ' . $value;
        }
    }
    return '<strong>' . $title . '</strong>'
        . (!empty($items) ? Html::ul($items, ['encode' => false]) : '<br />' . Module::t('common', 'None'));
}

/* @var $version Packagist\Api\Result\Package\Version */
?>

<div class="version">
    <div class="row">
        <div class="col-xs-12">
            <div class="media">
                <div class="media-left">
                    <i class="fa fa-copyright"></i>
                </div>
                <div class="media-body">
                    <?= implode(', ', $version->getLicense()) ?>
                    <i class="fa fa-bookmark"></i> <?= $version->getSource()->getReference() ?>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <i class="fa fa-user"></i>
                </div>
                <div class="media-body">
                    <?php
                        foreach($version->getAuthors() as $author) {
                            echo '<div>' . $author->getName() . ' <' . Html::mailto($author->getEmail()) . '></div>';
                        }
                    ?>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <i class="fa fa-tag"></i>
                </div>
                <div class="media-body">
                    <?= implode(' ', array_map(function($value) {
                        return Html::a('#' . $value, 'https://packagist.org/search?tags=' . $value, ['class' => 'margin-r-5']);
                    }, $version->getKeywords())) ?>
                </div>
            </div>
        </div>
    </div>
    
    <br />
    
    <div class="row">
        <div class="col-xs-4">
            <?= $fnUlItems($version->getRequire(), Module::t('common', 'requires')) ?>
        </div>
        <div class="col-xs-4">
            <?= $fnUlItems($version->getRequireDev(), Module::t('common', 'requires(dev)')) ?>
        </div>
        <div class="col-xs-4">
            <?= $fnUlItems(property_exists($version, 'suggest') ? $version->suggest : null, Module::t('common', 'suggests')) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-4">
            <?= $fnUlItems($version->getProvide(), Module::t('common', 'provides')) ?>
        </div>
        <div class="col-xs-4">
            <?= $fnUlItems($version->getConflict(), Module::t('common', 'conflicts')) ?>
        </div>
        <div class="col-xs-4">
            <?= $fnUlItems($version->getReplace(), Module::t('common', 'replaces')) ?>
        </div>
    </div>
</div>