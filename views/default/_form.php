<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wms\packages\Module;

/* @var $this yii\web\View */
/* @var $model wms\packages\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="module-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'module_status_id')->textInput() ?>

    <?= $form->field($model, 'module_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'module_package')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'module_settings')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'module_created_at')->textInput() ?>

    <?= $form->field($model, 'module_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Module::t('common', 'Create') : Module::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
