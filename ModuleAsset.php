<?php

namespace wms\packages;

class ModuleAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@wms/packages/assets';
    
    public $css = [
        'css/main.css',
    ];
    
    public $js = [
        'js/main.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}
