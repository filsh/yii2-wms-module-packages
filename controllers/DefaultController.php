<?php

namespace wms\packages\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use wms\packages\ModuleAsset;
use wms\packages\models\Search;
use wms\packages\models\Filter;
use wms\packages\components\FinderInterface;

class DefaultController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        ModuleAsset::register($this->view);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'install' => ['POST'],
                    'uninstall' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new Search();
        
        $filterModel = new Filter();
        $dataProvider = $filterModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'filterModel' => $filterModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return json_encode([
            'results' => $dataProvider->getModels()
        ]);
    }

    public function actionView($package, FinderInterface $finder)
    {
        return $this->render('view', [
            'package' => $finder->findByPackageName($package),
        ]);
    }
    
    public function actionInstall($package, FinderInterface $finder)
    {
        ;
    }
    
    public function actionUninstall($package, FinderInterface $finder)
    {
        ;
    }
}
