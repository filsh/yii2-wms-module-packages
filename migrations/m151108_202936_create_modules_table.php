<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\packages\models\Module as ModuleModel;
use wms\packages\models\Module\Status;

class m151108_202936_create_modules_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ModuleModel::tableName(), [
            'module_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'module_status_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'module_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'module_package' => Schema::TYPE_STRING . '(255) NOT NULL',
            'module_settings' => Schema::TYPE_TEXT . ' NOT NULL',
            'module_created_at' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'module_updated_at' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'FOREIGN KEY (`module_status_id`) REFERENCES ' . Status::tableName() . ' (`module_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ModuleModel::tableName());
    }
}
