<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\packages\models\Module\Status;

class m151108_202916_create_module_statuses_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Status::tableName(), [
            'module_status_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'module_status_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'module_status_title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'UNIQUE INDEX (`module_status_name`)'
        ], $tableOptions);
        
        $this->batchInsert(Status::tableName(), ['module_status_name', 'module_status_title'], [
            [Status::LOADED, 'Loaded'],
            [Status::INSTALLED, 'Installed'],
            [Status::REMOVED, 'Removed'],
            [Status::FAILED, 'Failed']
        ]);
    }

    public function down()
    {
        $this->dropTable(Status::tableName());
    }
}
